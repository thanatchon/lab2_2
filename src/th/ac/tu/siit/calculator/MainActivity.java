package th.ac.tu.siit.calculator;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {

	String output = "0";
	double input1 = 0.0;
	int operator = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		((Button) findViewById(R.id.ac)).setOnClickListener(this);
		((Button) findViewById(R.id.bs)).setOnClickListener(this);
		((Button) findViewById(R.id.dot)).setOnClickListener(this);
		((Button) findViewById(R.id.equ)).setOnClickListener(this);
		((Button) findViewById(R.id.add)).setOnClickListener(this);
		((Button) findViewById(R.id.sub)).setOnClickListener(this);
		((Button) findViewById(R.id.mul)).setOnClickListener(this);
		((Button) findViewById(R.id.div)).setOnClickListener(this);
		((Button) findViewById(R.id.num0)).setOnClickListener(this);
		((Button) findViewById(R.id.num1)).setOnClickListener(this);
		((Button) findViewById(R.id.num2)).setOnClickListener(this);
		((Button) findViewById(R.id.num3)).setOnClickListener(this);
		((Button) findViewById(R.id.num4)).setOnClickListener(this);
		((Button) findViewById(R.id.num5)).setOnClickListener(this);
		((Button) findViewById(R.id.num6)).setOnClickListener(this);
		((Button) findViewById(R.id.num7)).setOnClickListener(this);
		((Button) findViewById(R.id.num8)).setOnClickListener(this);
		((Button) findViewById(R.id.num9)).setOnClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		TextView outputTV = (TextView) findViewById(R.id.output);
		TextView op = (TextView) findViewById(R.id.operator);

		switch (id) {
		case R.id.num0:
		case R.id.num1:
		case R.id.num2:
		case R.id.num3:
		case R.id.num4:
		case R.id.num5:
		case R.id.num6:
		case R.id.num7:
		case R.id.num8:
		case R.id.num9:
			output += ((Button) v).getText().toString();

			if (output.charAt(0) == '0' && output.charAt(1) != '.')
				output = output.substring(1);

			if (output.length() <= 0)
				output = "0";

			outputTV.setText(output);
			break;

		case R.id.dot:
			output += ".";
			if (output == ".") {
				output = "0.";
			}
			outputTV.setText(output);
			break;

		case R.id.ac:
			output = "0"; /* Clears */
			input1 = 0.0;
			operator = 0;
			outputTV.setText(output);
			op.setText("");
			break;

		case R.id.bs:
			int l = output.length();
			if (l > 1) {
				output = output.substring(0, output.length() - 1);
			} else {
				output = "0";
			}
			outputTV.setText(output);
			break;

		case R.id.add:
			if (operator == 0) {
				operator = 1;
				input1 = Double.parseDouble(output);
			} else {
				calculate();
				outputTV.setText(output);
				operator = 1;
				input1 = Double.parseDouble(output);
			}
			output = "0";
			op.setText("+");
			break;
		case R.id.sub:
			if (operator == 0) {
				operator = 2;
				input1 = Double.parseDouble(output);
			} else {
				calculate();
				outputTV.setText(output);
				operator = 2;
				input1 = Double.parseDouble(output);
			}
			output = "0";
			op.setText("-");
			break;

		case R.id.mul:
			if (operator == 0) {
				operator = 3;
				input1 = Double.parseDouble(output);
			} else {
				calculate();
				outputTV.setText(output);
				operator = 3;
				input1 = Double.parseDouble(output);
			}
			output = "0";
			op.setText("*");
			break;

		case R.id.div:
			if (operator == 0) {
				operator = 4;
				input1 = Double.parseDouble(output);
			} else {
				calculate();
				outputTV.setText(output);
				operator = 4;
				input1 = Double.parseDouble(output);
			}
			output = "0";
			op.setText("�");
			break;

		case R.id.equ:
			calculate();
			outputTV.setText(output);
			operator = 0;
			op.setText("");
			break;
		}

	}

	void calculate() {
		double input2 = Double.parseDouble(output);
		if (operator == 1) {
			output = input1 + input2 + "";
		} else if (operator == 2) {
			output = input1 - input2 + "";
		} else if (operator == 3) {
			if (input2 == 0) {
				output = input1 * 1 + "";
			} else {
				output = input1 * input2 + "";
			}
		} else if (operator == 4) {
			if (input2 == 0) {
				output = input1 / 1 + "";
			} else {
				output = input1 / input2 + "";
			}
		}

	}

}